package ru.exsoft.MediaRetranslatorServer.server;

public enum Command {
    PLAYPAUSE, STOP, NEXT, PREVIOUS, VOLUME_UP, VOLUME_DOWN, CHECKPASS, SCREEN_OFF, SHUTDOWN, SHUTDOWN_CANCEL, LOCK
}
