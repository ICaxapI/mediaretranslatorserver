package ru.exsoft.MediaRetranslatorServer.server;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import org.jnativehook.GlobalScreen;
import org.jnativehook.keyboard.NativeKeyEvent;
import ru.exsoft.MediaRetranslatorServer.Utils.CryptUtils;

import java.awt.*;
import java.io.*;

public class ServerService extends Listener {
    private static Server server;
    private static String password;

    public static void start(int tcpPort, int udpPort, String password) throws Exception {
        ServerService.password = password;
        System.out.println("Creating server");
        server = new Server();
        server.getKryo().register(PacketMessage.class);
        server.getKryo().register(Command.class);
        server.bind(tcpPort, udpPort);
        server.start();
        server.addListener(new ServerService());
    }

    public void connected(Connection c) {
        System.out.println("Connected " + c.getRemoteAddressTCP().getHostString());
    }

    public void received(Connection c, Object p){
        if (p instanceof PacketMessage) {
            PacketMessage packet = (PacketMessage) p;
            if (CryptUtils.getMd5(password).equals(packet.password)) {
                switch (packet.command) {
                    case CHECKPASS:
                        System.out.println("CHECKPASS");
                        PacketMessage checkpass = new PacketMessage();
                        checkpass.command = Command.CHECKPASS;
                        checkpass.message = "ok";
                        checkpass.nano = packet.nano;
                        c.sendTCP(checkpass);
                        break;
                    case NEXT:
                        System.out.println("NEXT");
                        GlobalScreen.postNativeEvent(new NativeKeyEvent(2401,0,176,NativeKeyEvent.VC_MEDIA_NEXT, org.jnativehook.keyboard.NativeKeyEvent.CHAR_UNDEFINED));
                        PacketMessage next = new PacketMessage();
                        next.command = Command.NEXT;
                        next.message = "ok";
                        next.nano = packet.nano;
                        c.sendTCP(next);
                        break;
                    case PREVIOUS:
                        System.out.println("PREV");
                        GlobalScreen.postNativeEvent(new NativeKeyEvent(2401,0,177, NativeKeyEvent.VC_MEDIA_PREVIOUS, org.jnativehook.keyboard.NativeKeyEvent.CHAR_UNDEFINED));
                        PacketMessage prev = new PacketMessage();
                        prev.command = Command.PREVIOUS;
                        prev.message = "ok";
                        prev.nano = packet.nano;
                        c.sendTCP(prev);
                        break;
                    case PLAYPAUSE:
                        System.out.println("PLAYPAUSE");
                        GlobalScreen.postNativeEvent(new NativeKeyEvent(2401,0,179, NativeKeyEvent.VC_MEDIA_PLAY, org.jnativehook.keyboard.NativeKeyEvent.CHAR_UNDEFINED));
                        PacketMessage pp = new PacketMessage();
                        pp.command = Command.PLAYPAUSE;
                        pp.message = "ok";
                        pp.nano = packet.nano;
                        c.sendTCP(pp);
                        break;
                    case STOP:
                        System.out.println("PAUSE");
                        GlobalScreen.postNativeEvent(new NativeKeyEvent(2401,0,179, NativeKeyEvent.VC_MEDIA_STOP, org.jnativehook.keyboard.NativeKeyEvent.CHAR_UNDEFINED));
                        PacketMessage pause = new PacketMessage();
                        pause.command = Command.STOP;
                        pause.message = "ok";
                        pause.nano = packet.nano;
                        c.sendTCP(pause);
                        break;
                    case VOLUME_UP:
                        System.out.println("VUP");
                        GlobalScreen.postNativeEvent(new NativeKeyEvent(2401,0,175, NativeKeyEvent.VC_VOLUME_UP, org.jnativehook.keyboard.NativeKeyEvent.CHAR_UNDEFINED));
                        PacketMessage vup = new PacketMessage();
                        vup.command = Command.VOLUME_UP;
                        vup.message = "ok";
                        vup.nano = packet.nano;
                        c.sendTCP(vup);
                        break;
                    case VOLUME_DOWN:
                        System.out.println("VDOWN");
                        GlobalScreen.postNativeEvent(new NativeKeyEvent(2401,0,174, NativeKeyEvent.VC_VOLUME_DOWN, org.jnativehook.keyboard.NativeKeyEvent.CHAR_UNDEFINED));
                        PacketMessage vdwn = new PacketMessage();
                        vdwn.command = Command.VOLUME_DOWN;
                        vdwn.message = "ok";
                        vdwn.nano = packet.nano;
                        c.sendTCP(vdwn);
                        break;
                    case SCREEN_OFF:
                        System.out.println("SCREEN_OFF");
                        try {
                            String command = "./wincmnds/screenOff.lnk";
                            Desktop.getDesktop().open(new File(command));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        PacketMessage scoff = new PacketMessage();
                        scoff.command = Command.SCREEN_OFF;
                        scoff.message = "ok";
                        scoff.nano = packet.nano;
                        c.sendTCP(scoff);
                        break;
                    case SHUTDOWN:
                        System.out.println("SHUTDOWN");
                        try {
                            String command = "./wincmnds/shutdown.lnk";
                            Desktop.getDesktop().open(new File(command));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        PacketMessage shdwn = new PacketMessage();
                        shdwn.command = Command.SHUTDOWN;
                        shdwn.message = "ok";
                        shdwn.nano = packet.nano;
                        c.sendTCP(shdwn);
                        break;
                    case SHUTDOWN_CANCEL:
                        System.out.println("SHUTDOWN_CANCEL");
                        try {
                            String command = "./wincmnds/shutdownCancel.lnk";
                            Desktop.getDesktop().open(new File(command));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        PacketMessage shdwncncl = new PacketMessage();
                        shdwncncl.command = Command.SHUTDOWN_CANCEL;
                        shdwncncl.message = "ok";
                        shdwncncl.nano = packet.nano;
                        c.sendTCP(shdwncncl);
                        break;
                    case LOCK:
                        System.out.println("LOCK");
                        try {
                            String command = "./wincmnds/lock.lnk";
                            Desktop.getDesktop().open(new File(command));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        PacketMessage lock = new PacketMessage();
                        lock.command = Command.LOCK;
                        lock.message = "ok";
                        lock.nano = packet.nano;
                        c.sendTCP(lock);
                        break;
                }
            } else {
                PacketMessage packetMessage = new PacketMessage();
                packetMessage.command = Command.CHECKPASS;
                packetMessage.message = "error";
                packetMessage.nano = packet.nano;
                c.sendTCP(packetMessage);
            }
        }
    }

    public void disconnected(Connection c){System.out.println("Disconnected");}
}