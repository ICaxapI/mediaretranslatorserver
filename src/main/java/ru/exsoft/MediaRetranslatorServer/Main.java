package ru.exsoft.MediaRetranslatorServer;

import ru.exsoft.MediaRetranslatorServer.config.Config;
import ru.exsoft.MediaRetranslatorServer.config.ConfigManager;
import ru.exsoft.MediaRetranslatorServer.server.ServerService;

import java.io.File;

public class Main{
    public static final ConfigManager configManager = new ConfigManager();
    private static final File CONFIG_FILE = new File("./config.json");

    public static void main(String[] args) throws Exception {
        configManager.load(CONFIG_FILE);
        Config config = configManager.getConfig();
        ServerService.start(config.tcpPort, config.udpPort, config.password);
    }

}